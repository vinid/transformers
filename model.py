import numpy as np
import torch
import torch.nn as nn


class PositionalEncoding(nn.Module):
    """
    Positional Encoding for the Transformer
    """

    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)
        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-np.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        pe = pe.unsqueeze(0).transpose(0, 1)
        self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :]
        return self.dropout(x)


class TransformerNet(nn.Module):
    """
    Network based on the transformer
    """
    def __init__(self, num_src_vocab, embedding_dim, hidden_size, nheads, n_layers, max_src_len, dropout):
        super(TransformerNet, self).__init__()
        # embedding layers
        self.enc_embedding = nn.Embedding(num_src_vocab, embedding_dim)

        # positional encoding layers
        self.enc_pe = PositionalEncoding(embedding_dim, max_len=max_src_len)

        # encoder/decoder layers
        enc_layer = nn.TransformerEncoderLayer(embedding_dim, nheads, hidden_size, dropout)

        self.encoder = nn.TransformerEncoder(enc_layer, num_layers=n_layers)

        # final dense layer
        self.dense = nn.Linear(embedding_dim, num_src_vocab)
        self.log_softmax = nn.LogSoftmax()

    def forward(self, src):
        src = self.enc_embedding(src).permute(1, 0, 2)
        src = self.enc_pe(src)
        memory = self.encoder(src)
        final_out = self.dense(memory)
        return self.log_softmax(final_out)

    def get_embedding(self, src):
        src = self.enc_embedding(src).permute(1, 0, 2)
        src = self.enc_pe(src)
        memory = self.encoder(src)
        return memory
