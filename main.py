from tqdm import tqdm
from data import create_dataloader
from model import TransformerNet
from configparser import ConfigParser

from torch import nn
import torch

training_dataloader, vocab = create_dataloader()

vocab_size = len(vocab.tokens)
configur = ConfigParser()
configur.read('config.ini')
max_sent_len = int(configur.get("keys", "sentence_max_length"))

embedding_dim = int(configur.get("keys", "embedding_dim"))
hidden_size = int(configur.get("keys", "hidden_size"))
num_heads = int(configur.get("keys", "num_heads"))
num_layers = int(configur.get("keys", "num_layers"))
dropout = float(configur.get("keys", "dropout"))
learning_rate = float(configur.get("keys", "learning_rate"))
num_epochs = int(configur.get("keys", "num_epochs"))
device = (configur.get("keys", "device"))

model = TransformerNet(vocab_size, embedding_dim, hidden_size, num_heads, num_layers, max_sent_len, dropout).to(device)
criterion = nn.CrossEntropyLoss(ignore_index=vocab.tokens.index("<pad>"))
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

loss_trace = []

pbar = tqdm(total=(num_epochs))

for epoch in range(num_epochs):
    current_loss = 0
    for i, (x, y) in enumerate(training_dataloader):
        x, y = x.to(device), y.to(device)
        outputs = model(x)
        loss = criterion(outputs.permute(1, 2, 0), y)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        pbar.update(1)
        pbar.set_description(str(loss.item()))
        current_loss += loss.item()

        loss_trace.append(current_loss)
pbar.close()