from tqdm import tqdm
import numpy as np
import torch
from configparser import ConfigParser


class Vocab:
    def __init__(self, tokens):
        self.tokens = tokens

    def sentence_to_ids(self, text):
        return [self.tokens.index(t) for t in text]


class MTDataset(torch.utils.data.Dataset):
    def __init__(self, source_sentences, target_sentences):
        self.source = np.array(source_sentences, dtype=int)
        self.target = np.array(target_sentences, dtype=int)

    def __getitem__(self, idx):
        return self.source[idx], self.target[idx]

    def __len__(self):
        # returns len source of data
        return len(self.source)


def create_dataloader():
    with open("data/maxi_masked.txt") as f:
        sentences = list(map(lambda x: x.replace("\n", ""), f.readlines()))

    configur = ConfigParser()
    configur.read('config.ini')
    max_sent_len = int(configur.get("keys", "sentence_max_length"))

    source_sentences, target_sentences = [], []
    tokens = set()

    pbar = tqdm(total=len(sentences))
    for sent in sentences:

        source_sent, target_sent = ["<sos>"], ["<sos>"]

        source_sent += sent.split("#")[0].split()
        target_sent += sent.split("#")[1].split()
        pbar.update(1)
        # change to lowercase
        source_sent = [x.lower() for x in source_sent]
        target_sent = [x.lower() for x in target_sent]

        if len(source_sent) >= max_sent_len:
            source_sent = source_sent[:max_sent_len]
        else:
            for _ in range(max_sent_len - len(source_sent)):
                source_sent.append("<pad>")

        if len(target_sent) >= max_sent_len:
            target_sent = target_sent[:max_sent_len]
        else:
            for _ in range(max_sent_len - len(target_sent)):
                target_sent.append("<pad>")

        # add parsed sentences
        source_sentences.append(source_sent)
        target_sentences.append(target_sent)

        # update unique words
        tokens.update(source_sent)
        tokens.update(target_sent)
    pbar.close()

    tokens = list(tokens)

    # encode each token into index
    for i in tqdm(range(len(source_sentences))):
        source_sentences[i] = [tokens.index(x) for x in source_sentences[i]]
        target_sentences[i] = [tokens.index(x) for x in target_sentences[i]]

    dataset = MTDataset(source_sentences, target_sentences)

    train_loader = torch.utils.data.DataLoader(dataset, batch_size=200)
    return train_loader, Vocab(tokens=tokens)


