import random

with open("data/maxi_sessions.txt") as filino:
    data = list(map(lambda x : x.replace("\n", ""), filino.readlines()))

with open("data/maxi_masked.txt", "w") as filino:
    for i in range(0, 10):
        for a in data:
            splitted = (a.split())
            if len(splitted) > 20:
                continue
            if len(splitted) < 4:
                continue
            else:
                masked = random.randint(0, len(splitted) - 1)
                masked_version = ["<pad>"]*len(splitted)
                masked_version[masked] = splitted[masked]
                splitted[masked] = "mask"

                filino.write(" ".join(splitted) + "#" + " ".join(masked_version) + "\n")



